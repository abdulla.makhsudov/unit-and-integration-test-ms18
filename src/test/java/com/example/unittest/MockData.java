package com.example.unittest;

import com.example.unittest.model.Transfer;

public class MockData {

    public static final Transfer user1 = Transfer.builder()
            .id(1L)
            .name("Mustafa")
            .balance(300.00)
            .build();

    public static final Transfer user2 = Transfer.builder()
            .id(2L)
            .name("Senan")
            .balance(400.00)
            .build();
}

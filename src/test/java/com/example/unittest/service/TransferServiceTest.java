package com.example.unittest.service;

import static com.example.unittest.MockData.user1;
import static com.example.unittest.MockData.user2;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.example.unittest.model.Transfer;
import com.example.unittest.repo.TransferRepo;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest(classes = {
        TransferService.class,
        TransferRepo.class
})
public class TransferServiceTest {

    @Autowired
    public TransferService transferService;

    @MockBean
    public TransferRepo transferRepo;

    @Test
    void givenBalanceTransferOtherAmountWhenGetSuccess() {

        // Arrange
        Transfer result = Transfer.builder()
                .balance(400.00)
                .name("Senan")
                .build();

        when(transferRepo.findById(1L)).thenReturn(Optional.ofNullable(user1));
        when(transferRepo.findById(2L)).thenReturn(Optional.ofNullable(user2));

        // Assert // Act
        assertEquals(result.getName(), transferService.transfer(user1, user2).getName());
        assertEquals(result.getBalance(), transferService.transfer(user1, user2).getBalance());
    }

    @Test
    void givenBalanceTransferOtherAmountWhenGetSuccess2() {

        // Arrange
        Transfer result = Transfer.builder()
                .id(2L)
                .balance(400.00)
                .name("Senan")
                .build();

        when(transferRepo.findById(1L)).thenReturn(Optional.ofNullable(user1));
        when(transferRepo.findById(2L)).thenReturn(Optional.ofNullable(user2));

        // Assert // Act
        assertEquals(result, transferService.transfer(user1, user2));
        verify(this.transferRepo, times(2)).findById(any(Long.class));
    }
}

package com.example.unittest.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CalculateTest {

    @InjectMocks
    public Calculate calculate;

//    @BeforeEach
//    void setup() {
//
//    }


    @Test
    void givenTwoParametrsWhenMultiplyThenSuccess() {

        // Arrange
        int a = 5;
        int b = 7;
        double result = 35;

        // Act, Assert
        Assertions.assertEquals(result, calculate.multiply(a, b));
    }

    @Test
    void givenTwoParametrsWhenDivideThenSuccess() {

        // Arrange
        int a = 15;
        int b = 3;
        double result = 5;

        // Act, Assert
        Assertions.assertEquals(result, calculate.divide(a, b));
    }

    @Test
    void givenTwoParametrsWhenMultiplyThenFailure() {

        // Arrange
        int a = 15;
        int b = 0;

        // Act, Assert
        RuntimeException exception = assertThrows(RuntimeException.class, () -> calculate.divide(a, b));
        assertTrue(exception.getMessage().equals("b not equals zero"));
    }


}

package com.example.unittest.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.example.unittest.model.Transfer;
import com.example.unittest.repo.TransferRepo;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class TransferServiceIntegrationTest {

    @Autowired
    public TransferRepo transferRepo;

    @Container
    public static PostgreSQLContainer postgres = new PostgreSQLContainer("postgres")
            .withDatabaseName("transfer")
            .withUsername("postgres")
            .withPassword("password");

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry dynamicPropertyRegistry) {
        dynamicPropertyRegistry.add("spring.datasource.url", postgres::getJdbcUrl);
        dynamicPropertyRegistry.add("spring.datasource.username", postgres::getUsername);
        dynamicPropertyRegistry.add("spring.datasource.password", postgres::getPassword);
        dynamicPropertyRegistry.add("spring.datasource.databasename", postgres::getDatabaseName);
        dynamicPropertyRegistry.add("spring.datasource.driver-class-name", postgres::getDriverClassName);
    }

    @BeforeAll
    static void start() {
        postgres.start();
    }

    @Test
    void saveTransferTest() {
        Transfer transfer = Transfer.builder()
                .balance(300)
                .name("Kamil")
                .build();

        Transfer saveTransfer = transferRepo.save(transfer);

        assertEquals(2L, saveTransfer.getId());
    }


    @AfterAll
    static void stop() {
        postgres.stop();
    }


}

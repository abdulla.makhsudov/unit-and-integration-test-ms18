package com.example.unittest.controller;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.unittest.model.Transfer;
import com.example.unittest.service.TransferService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(TransferController.class)
public class TransferControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    public TransferService transferService;

    @Test
    void getTransferByIdTest() throws Exception {

        //Arrange
        Transfer transfer = Transfer.builder()
                .id(2L)
                .balance(400)
                .name("Senan")
                .build();

        when(transferService.getTransferById(2L)).thenReturn(transfer);

        mockMvc.perform(get("/transfer/2")
                        .content(String.valueOf(APPLICATION_JSON)))
//                .andExpect(content().json(objectToJson(transfer)))
                .andExpect(jsonPath("$.id").value(2L))
                .andExpect(jsonPath("$.name").value("Senan"))
                .andExpect(jsonPath("$.balance").value(400))
                .andExpect(header().exists("hello"))
                .andExpect(status().isOk());
    }


    @Test
    void saveTransferTest() throws Exception {

        //Arrange
        Transfer savedTransfer = Transfer.builder()
                .id(1L)
                .balance(400)
                .name("Senan")
                .build();

        when(transferService.saveTransfer(any())).thenReturn(savedTransfer);

        mockMvc.perform(post("/transfer")
                        .content(objectToJson(new Transfer(null, 400, "Senan")))
                        .contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(status().isCreated());
    }

    public String objectToJson(Object object) throws JsonProcessingException {
        return objectMapper.writeValueAsString(object);
    }

}

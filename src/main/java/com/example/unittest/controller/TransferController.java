package com.example.unittest.controller;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import com.example.unittest.model.Transfer;
import com.example.unittest.service.TransferService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transfer")
@RequiredArgsConstructor
public class TransferController {

    private final TransferService transferService;

    @GetMapping("/{id}")
    public ResponseEntity<Transfer> getTransferById(@PathVariable Long id) {
        return ResponseEntity.status(OK)
                .header("hello", "salam")
                .body(transferService.getTransferById(id));
    }

    @PostMapping
    public ResponseEntity<Transfer> saveTranser(@RequestBody Transfer transfer) {
        return ResponseEntity.status(CREATED)
                .body(transferService.saveTransfer(transfer));
    }


}

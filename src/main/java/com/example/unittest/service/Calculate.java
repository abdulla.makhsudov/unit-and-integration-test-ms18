package com.example.unittest.service;

import org.springframework.stereotype.Service;

@Service
public class Calculate {

    public double multiply (int a, int b) {
        return a * b;
    }

    public double divide(int a, int b) {
        if (b == 0) {
            throw new RuntimeException("b not equals zero");
        }
        return a / b;
    }


}

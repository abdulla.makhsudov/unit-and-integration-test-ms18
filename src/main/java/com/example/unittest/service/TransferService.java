package com.example.unittest.service;

import com.example.unittest.model.Transfer;
import com.example.unittest.repo.TransferRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class TransferService {

    private final TransferRepo transferRepo;

    @Transactional
    public Transfer transfer(Transfer user1, Transfer user2) {
        Transfer transfer1 = transferRepo.findById(user1.getId()).get();
        Transfer transfer2 = transferRepo.findById(user2.getId()).get();
        transfer1.setBalance(transfer1.getBalance() - transfer1.getBalance());
        transfer2.setBalance(transfer1.getBalance() + transfer2.getBalance());
        return user2;
    }

    public Transfer getTransferById(Long id) {
        return transferRepo.findById(id).orElseThrow(() -> new RuntimeException("Transfer not found"));
    }

    public Transfer saveTransfer(Transfer transfer) {
        return transferRepo.save(transfer);
    }
}
